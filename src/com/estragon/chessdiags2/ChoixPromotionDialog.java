package com.estragon.chessdiags2;

import ressources.Ressources;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import chesspresso.Chess;

public class ChoixPromotionDialog extends DialogFragment implements android.widget.AdapterView.OnItemClickListener {

	public static String TAG = "DateDialogFragment";
	static private int[] conversion = new int[] {0,2,0,3,5,4};
	static private int[] conversionPieces = new int[] {Chess.NO_PIECE, Chess.KNIGHT, Chess.NO_PIECE , Chess.BISHOP, Chess.QUEEN ,Chess.ROOK};

	static Context context;
	static PromotionCallback callBack;

	public static ChoixPromotionDialog newInstance(Context context, PromotionCallback callBack, int couleurDefendue){
		ChoixPromotionDialog dialog  = new ChoixPromotionDialog();

		ChoixPromotionDialog.context = context;
		ChoixPromotionDialog.callBack = callBack;
		
		Bundle args = new Bundle();
		args.putInt("couleurDefendue", couleurDefendue);
		dialog.setArguments(args);
		return dialog;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		ChoixPromotionWidget choixPromotionWidget;
		AlertDialog dialog = new AlertDialog.Builder(context).setTitle(R.string.promotion).setView(choixPromotionWidget = new ChoixPromotionWidget(context,getArguments().getInt("couleurDefendue", 0))).setNegativeButton(R.string.cancel,null).create();
		choixPromotionWidget.setOnItemClickListener(this);
		return dialog;
	}
	
	

	@Override
	public void onSaveInstanceState(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(arg0);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position,
			long arg3) {
		// TODO Auto-generated method stub
		dismiss();
		callBack.onChoixFait(conversionPieces[position]);
	}

	public static interface PromotionCallback {
		public void onChoixFait(int promotion);
	}

	public class ChoixPromotionWidget extends GridView {

		public ChoixPromotionWidget(Context context,int couleurDefendue) {
			super(context);
			setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));
			setNumColumns(3);
			setAdapter(new ImageAdapter(context,couleurDefendue));
		}




	}

	public static class ImageAdapter extends BaseAdapter {



		Context context;
		int couleurDefendue;

		public ImageAdapter(Context context, int couleurDefendue) {
			this.context = context;
			this.couleurDefendue = couleurDefendue;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return conversion.length;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			ImageView imageView;
			if (convertView == null) {  // if it's not recycled, initialize some attributes
				imageView = new ImageView(context);
				imageView.setLayoutParams(new GridView.LayoutParams(150, 150));
				imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
				//imageView.setPadding(8, 8, 8, 8);
			} else {
				imageView = (ImageView) convertView;
			}

			if (conversion[position] == 0) {
				return imageView;
			}
			
			if (couleurDefendue == Chess.WHITE) {
				imageView.setImageBitmap(Ressources.pieces[conversion[position]]);
			}
			else {
				imageView.setImageBitmap(Ressources.pieces[conversion[position] + 6]);
			}
			
			return imageView;
		}

		@Override
		public boolean isEnabled(int position) {
			// TODO Auto-generated method stub
			return conversion[position] != 0;
		}



	}
}